class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :customer, index: true
      t.references :employee, index: true
      t.date :date
      t.float :amount
      t.string :square_ref

      t.timestamps
    end
  end
end
