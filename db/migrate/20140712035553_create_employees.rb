class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :fname
      t.string :lname
      t.string :email
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.string :password
      t.date :pass_date
      t.boolean :manager

      t.timestamps
    end
  end
end
