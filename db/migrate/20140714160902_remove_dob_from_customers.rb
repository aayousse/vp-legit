class RemoveDobFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :dob, :date
  end
end
