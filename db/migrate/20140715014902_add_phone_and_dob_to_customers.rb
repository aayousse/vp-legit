class AddPhoneAndDobToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :phone2, :string
    add_column :customers, :dob, :string
  end
end
