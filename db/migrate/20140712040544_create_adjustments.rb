class CreateAdjustments < ActiveRecord::Migration
  def change
    create_table :adjustments do |t|
      t.references :customer, index: true
      t.references :employee, index: true
      t.string :reason

      t.timestamps
    end
  end
end
