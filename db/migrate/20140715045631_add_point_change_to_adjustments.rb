class AddPointChangeToAdjustments < ActiveRecord::Migration
  def change
    add_column :adjustments, :change, :integer
  end
end
