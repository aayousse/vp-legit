# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->

	updateWorth = ->
		discount = $('#discount').val() / 100;
		worth = $(this).val() * discount
		$('#redemption_worth').html(worth.toFixed(2))

	$('#redemption_amount').keyup updateWorth
	$('#redemption_amount').change updateWorth
