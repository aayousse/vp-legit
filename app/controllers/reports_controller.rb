class ReportsController < ApplicationController

	before_action :must_be_manager

	def home
	end

	def purchases
		if params[:commit] || params[:csv]
			@purchases = Purchase.all
			@title = "Viewing purchases"
			from, to, employee = [params[:from], params[:to], params[:employee]]
			
			unless from.blank?
				@purchases = @purchases.where("date >= ?", from)
				@title += " from #{from}"
			end

			unless to.blank?
				@purchases = @purchases.where("date <= ?", to)
				@title += " to #{to}"
			end

			unless employee.blank?
				@purchases = @purchases.where("employee_id = ?", employee)
				@title += " with employee #{Employee.find_by_id(employee)}"
			end

			@purchases = @purchases.order("date DESC")

			if params[:csv]
				send_data Purchase.report(@purchases), filename: "purchases_report.csv"
			else
				render :purchases_results
			end
		else
			render :purchases
		end
	end

	def redemptions
		if params[:commit] || params[:csv]
			@redemptions = Redemption.all
			@title = "Viewing redemptions"
			from, to, employee = [params[:from], params[:to], params[:employee]]
			
			unless from.blank?
				@redemptions = @redemptions.where("date >= ?", from)
				@title += " from #{from}"
			end

			unless to.blank?
				@redemptions = @redemptions.where("date <= ?", to)
				@title += " to #{to}"
			end

			unless employee.blank?
				@redemptions = @redemptions.where("employee_id = ?", employee)
				@title += " with employee #{Employee.find_by_id(employee)}"
			end

			@redemptions = @redemptions.order("date DESC")

			if params[:csv]
				send_data Redemption.report(@redemptions), filename: "redemptions_report.csv"
			else
				render :redemptions_results
			end
		else
			render :redemptions
		end
	end

	def adjustments
		if params[:commit] || params[:csv]
			@adjustments = Adjustment.all
			@title = "Viewing adjustments"
			from, to, reason = [params[:from], params[:to], params[:reason]]
			
			unless from.blank?
				@adjustments = @adjustments.where("date >= ?", from)
				@title += " from #{from}"
			end

			unless to.blank?
				@adjustments = @adjustments.where("date <= ?", to)
				@title += " to #{to}"
			end

			unless reason.blank?
				@adjustments = @adjustments.where("reason = ?", reason)
				@title += " with reason \"#{reason}\""
			end

			@adjustments = @adjustments.order("created_at DESC")

			if params[:csv]
				send_data Adjustment.report(@adjustments), filename: "adjustments_report.csv"
			else
				render :adjustments_results
			end
		else
			render :adjustments
		end
	end

	def customers
		if params[:commit] || params[:csv]
			@customers = Customer.all
			@title = "Viewing customers"
			from = params[:from]
			
			unless from.blank?
				@customers = Purchase.where("date > ?", from).map { |purchase| purchase.customer }.uniq
				@title += " with purchases more recent than #{from}"
			end

			if params[:csv]
				send_data Customer.report(@customers), filename: "customers_report.csv"
			else
				render :customers_results
			end
		else
			render :customers
		end
	end

end
