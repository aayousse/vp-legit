class SessionsController < ApplicationController

	def home
		redirect_to current_user if signed_in_customer?
		render 'static_pages/manager_home' if signed_in_manager?
		render 'static_pages/associate_home' if signed_in_associate?
		render :new if not signed_in?
	end

	def new
		redirect_to root_url if signed_in?
	end

	def create
		email = params[:session][:email].downcase

		customer = Customer.find_by(email: email)
		if customer && customer.authenticate(params[:session][:password])
			sign_in customer, :customer
			redirect_to customer
		else
			employee = Employee.find_by(email: email)
			if employee && employee.authenticate(params[:session][:password])
				sign_in employee, :employee
				redirect_to :home
			else
				flash.now[:error] = 'Invalid email/password combination.'
				render :new
			end
		end
	end

	def destroy
		if signed_in?
			sign_out current_user
			@signed_in_status = "Not signed in"
		end
		render :new
	end

end
