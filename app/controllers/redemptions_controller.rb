class RedemptionsController < ApplicationController
  before_action :set_redemption, only: [:show, :edit, :update, :destroy]
  before_action :must_be_employee

  # GET /redemptions
  # GET /redemptions.json
  def index
    @redemptions = Redemption.all
  end

  # GET /redemptions/1
  # GET /redemptions/1.json
  def show
  end

  # GET /redemptions/new
  def new
    if params[:customer_id]
      @customer = Customer.find_by_id(params[:customer_id])
      @customer && @redemption = @customer.redemptions.build
    else
      flash.now[:error] = "Something is wrong."
      redirect_to home_path; return
    end
    @redemption.employee = current_user
  end

  # GET /redemptions/1/edit
  def edit
  end

  # POST /redemptions
  # POST /redemptions.json
  def create
    @redemption = Redemption.new(redemption_params)

    @customer = @redemption.customer

    if params[:customer][:password] != @customer.password
      flash.now[:error] = 'Incorrect password for customer.'
      render :new; return
    end

    points_redeemed = @redemption.amount
    old_points_balance = @customer.current_points
    new_points_balance = old_points_balance - points_redeemed

    if new_points_balance < 0
      flash.now[:error] = 'Not enough customer points'
      render :new; return
    end

    @customer.update(current_points: new_points_balance)

    respond_to do |format|
      if @redemption.save
        format.html { redirect_to @redemption.customer, notice: 'Redemption successful; points deducted.' }
        format.json { render :show, status: :created, location: @redemption }
      else
        format.html { render :new }
        format.json { render json: @redemption.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /redemptions/1
  # PATCH/PUT /redemptions/1.json
  def update
    respond_to do |format|
      if @redemption.update(redemption_params)
        format.html { redirect_to @redemption, notice: 'Redemption was successfully updated.' }
        format.json { render :show, status: :ok, location: @redemption }
      else
        format.html { render :edit }
        format.json { render json: @redemption.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /redemptions/1
  # DELETE /redemptions/1.json
  def destroy
    @redemption.destroy
    respond_to do |format|
      format.html { redirect_to redemptions_url, notice: 'Redemption was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def update_discount
    Redemption.discount = params[:discount]
    redirect_to home_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_redemption
      @redemption = Redemption.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def redemption_params
      params.require(:redemption).permit(:customer_id, :employee_id, :date, :amount)
    end
end
