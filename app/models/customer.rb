class Customer < ActiveRecord::Base

	has_many :purchases
	has_many :redemptions
	has_many :adjustments

	after_initialize :init

	# Validations
	validates :fname, presence: true, length: {maximum: 25}
	validates :lname, presence: true, length: {maximum: 25}
	validates :email, presence: true, length: {maximum: 50}
	validates :email2, length: {maximum: 50}
	validates :email3, length: {maximum: 50}
	validates :address, length: {maximum: 25}
	validates :city, length: {maximum: 25}
	validates :state, length: {is: 2}, allow_blank: true
	validates :zip, length: {is: 5}, allow_blank: true
	validates :twitter, length: {maximum: 30}
	validates :dob, length: {is: 10}, allow_blank: true
	validates :dlnumber, length: {maximum: 8}
	validates :lifetime_points, presence: true, length: {maximum: 10}
	validates :current_points, presence: true, length: {maximum: 10}
	validates :password, presence: true, length: {maximum: 10}


	def authenticate(pass)
		pass == self.password
	end

	def to_s
		"#{self.fname} #{self.lname}"
	end

  def self.report(customers, options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      customers.each do |customer|
        csv << customer.attributes.values_at(*column_names)
      end
    end
  end

	private

		def init
			self.lifetime_points ||= 0
			self.current_points ||= 0
			self.city ||= "Houston"
			self.state ||= "TX"
			self.password ||= "12345"
		end

end
