class Adjustment < ActiveRecord::Base
  belongs_to :customer
  belongs_to :employee

  # Validations
  validates :customer_id, presence: true
  validates :employee_id, presence: true
  validates :reason, presence: true

  @@reasons = [
  	"Item Return",
  	"Customer Service Adjustment",
  	"Friends & Family",
  	"Points Expiration"
  ]

  def self.reasons
  	@@reasons
  end

  def self.report(adjustments, options = {})
    CSV.generate(options) do |csv|
      csv << ["Customer First Name", "Customer Last Name", "Customer Email",
        "Change", "Employee Last Name", "Date"]
      adjustments.each do |adjustment|
        csv << [adjustment.customer.fname, adjustment.customer.lname,
          adjustment.customer.email, adjustment.change,
          adjustment.employee.lname, adjustment.created_at.strftime("%m/%d/%Y")]
      end
    end
  end
end
