class Employee < ActiveRecord::Base

	after_initialize :init

	# Validations
	validates :fname, presence: true, length: {maximum: 25}
	validates :lname, presence: true, length: {maximum: 25}
	validates :email, presence: true, length: {maximum: 50}
	validates :address, presence: true, length: {maximum: 25}
	validates :city, presence: true, length: {maximum: 25}
	validates :state, length: {is: 2}
	validates :zip, length: {is: 5}
	validates :password, presence: true, length: {maximum: 10}

	def authenticate(pass)
		self.password == pass
	end

	def to_s
		"#{self.fname} #{self.lname}"
	end

	private
		def init
			self.city ||= "Houston"
			self.state ||= "TX"
			self.manager ||= false
		end

end
