module SessionsHelper

	def sign_in(user, role)
		session[:user] = user.id
		session[:role] = role
		self.current_user = user
	end

	def sign_out(user)
		session[:user] = nil
		session[:role] = nil
		self.current_user = nil
	end

	def current_user=(user)
		@current_user =  user
	end

	def current_user
		if session[:user] && session[:role] == 'customer'
			@current_user ||= Customer.find(session[:user])
		elsif session[:user] && session[:role] == 'employee'
			@current_user ||= Employee.find(session[:user])
		end
	end

	def signed_in?
		!current_user.nil?
	end

	def signed_in_customer?
		signed_in? && session[:role] == 'customer'
	end

	def signed_in_employee?
		signed_in? && session[:role] == 'employee'
	end

	def signed_in_associate?
		signed_in_employee? && !current_user.manager
	end	

	def signed_in_manager?
		signed_in_employee? && current_user.manager
	end	

	def must_be_signed_in
		if not signed_in?
			flash[:error] = "You must be signed in to view that page."
			redirect_to home_path
		end
	end

	def must_be_employee
		if not signed_in_employee?
			flash[:error] = "You must be an employee to view that page."
			redirect_to home_path
		end
	end

	def must_be_manager
		if not signed_in_manager?
			flash[:error] = "You must be a manager to view that page."
			redirect_to home_path
		end
	end

end
